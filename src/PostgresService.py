import psycopg2
import pandas as pd

class PostgresService(object):
    def __init__(self, config):
        self.config = config
        self.createConnection()

    def createConnection(self):
        '''
        Esta funcion crea la conexion
        '''
        self.conn = psycopg2.connect(host=self.config["host"], port=self.config["port"],
                                     database=self.config["database"], user=self.config["user"], password=self.config["password"])
        self.cur = self.conn.cursor()

    def queryDB(self, sql_query):
        '''
        Esta puncion hace una consulta sql a postgres y converte el resultado a un dataframe de pandas
        '''
        table = pd.read_sql_query(sql_query, self.conn)
        return table

    def closeConnection(self):
        '''
        Esta funcion cierra la conexion
        '''
        self.cur.close()
        self.conn.close()
