
# 0- Cita Asignada 1- Cita Confirmada 2- Cita cancelada 3- Cita ingresada por recepción 4- Cita guardada 5- Cita firmada 6- Cita facturada
dictionary_appointment = {
    0: 'Cita Asignada',
    1: 'Cita Confirmada',
    2: 'Cita Cancelada',
    3: 'Cita Ingresada por recepcion',
    4: 'Cita Guardada',
    5: 'Cita Firmada',
    6: 'Cita Facturada'
}
