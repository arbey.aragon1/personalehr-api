class QueryAppointmentsService(object):
    def __init__(self, postgres_service):
        self.postgres_service = postgres_service

    def queryAppointmentsByIdCompany(self, idCompany):
        '''
        Crea query para hacer la consulta de la cantidad y top de citas por id de la compañia
        '''
        oneHotQuery = ''
        for i in range(7):
            oneHotQuery += '''        SUM(CASE a.status
                WHEN '{0}' THEN 1
                ELSE 0
                END) AS status_{0},'''.replace('{0}', str(i))

        query = '''
        SELECT 
            {0}
            COUNT(*) AS total_appointment,
            c.id as id_company, 
            c.code, 
            LOWER(c.company_name) as name_lower,
            c.company_name
        FROM public.company c
            JOIN public.appointment a ON c.id = a.id_company_id
        WHERE {1} = c.id
        GROUP BY id_company
        '''.replace('{0}', oneHotQuery).replace('{1}', str(idCompany))
        print(query)
        return self.postgres_service.queryDB(query)

    def queryAppointmentsByIdUser(self, idUser=801):
        '''
        Todas las citas por usuario
        '''

        query = '''SELECT * FROM public.appointment a WHERE a.id_patient_id = {0}'''.replace(
            '{0}', str(idUser))

        print(query)
        return self.postgres_service.queryDB(query)
