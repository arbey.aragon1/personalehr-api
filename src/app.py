from utils import dictionary_appointment
from configuration import config
from PostgresService import PostgresService
from QueryAppointmentsService import QueryAppointmentsService

from flask import Flask
from flask import render_template
from flask import jsonify
from flask import send_from_directory, current_app, request

from docx import Document
from docx.shared import Inches
import pandas as pd 
from docx import Document
import subprocess

import pycurl
from io import BytesIO
import requests

app = Flask(__name__, static_folder='temporal')

postgres_service = PostgresService(config)

qaservice = QueryAppointmentsService(postgres_service)


def docxToPDF(docxPath, pdfPath):
    files = {'files': open(docxPath, 'rb')}
    response = requests.post('http://libreoffice:3000', files=files)#.prepare().body.decode('ascii')
    data = response.content
    with open(pdfPath, 'wb') as s:
        s.write(data)


def generateDOCX(templatePath, outputPath, df):
    document = Document(templatePath)

    dicKeys = {
        '{title}': 'Historia clinica'
    }

    for paragraph in document.paragraphs:
        for k in dicKeys:
            if k in paragraph.text:
                paragraph.text = paragraph.text.replace(k, dicKeys[k])

    rowsn, colsn = df.shape
    table = document.add_table(rows=rowsn, cols=colsn+1)
    hdr_cells = table.rows[0].cells

    hdr_cells[0].text = 'index'
    for i, c in enumerate(df.columns):
        #columna
        hdr_cells[i+1].text = str(c)

    for index, row in df.iterrows():
        row_cells = table.add_row().cells
        row_cells[0].text = str(index)
        for i, c  in enumerate(df.columns):
            row_cells[i+1].text = str(row[c])

    document.add_page_break()
    document.save(outputPath)


@app.route('/')
def queryAppointmentsByIdUser():
    data = qaservice.queryAppointmentsByIdUser().astype(str).to_dict('index')
    print(data)
    return jsonify(data)


@app.route('/test')
def test():
    return 'test'


@app.route('/appointments/<id>')
def appointment(id):
    data = qaservice.queryAppointmentsByIdUser(
        int(id)).astype(str).to_dict('index')
    return jsonify(data)



@app.route('/appointments-pdf/<idUser>', methods=['GET', 'POST'])
def appointmentPdf(idUser):
    data = qaservice.queryAppointmentsByIdUser(
        int(idUser)).astype(str)[['id_doctor_id', 'app_date', 'start_time', 'end_time', 'app_type']]
    
    df = data
    pdfFileName = "appointments_"+str(idUser)+".pdf"
    docx1Path = "./templates-pdf/AAMMII_TEST.docx"
    docx2Path = "./temporal/appointments_"+str(idUser)+".docx"
    pdfPath = "./temporal/"+pdfFileName
    generateDOCX(docx1Path, docx2Path, df)
    docxToPDF(docx2Path, pdfPath)

    return send_from_directory(directory='temporal', filename=pdfFileName)

@app.route('/template')
def template():
    return render_template('home.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
